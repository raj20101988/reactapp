//import {createStore, applyMiddleware} from 'redux';
import { createStore , applyMiddleware } from 'redux';

import  reducers  from '../reducers/userReducer.js'
//import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';

export default function configureStore() {
  return createStore(
    reducers , applyMiddleware(thunk)
  );
}



// import { createStore , combineReducers , applyMiddleware } from 'redux';
// import logger from 'redux-logger';
// import thunk from 'redux-thunk';
// import math from './reducers/mathReducer';
// import user from './reducers/userReducer';
// import car from './reducers/carReducer';

// const myLogger=(store)=>(next)=>(action)=>{
// 	console.log('logged action '+ action);
// 	next(action);
// }

// export default createStore(combineReducers({math , user , car}),{},
// 	applyMiddleware(myLogger,logger,thunk));