import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import App from './components/App.js';
import { BrowserRouter, Switch, Route, NavLink, Link } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore.js';

const store = configureStore();


//import {loadUsers} from './actions/userActions.js';
//store.dispatch(loadUsers());
//loadUsers();

ReactDOM.render((<Provider store={store}>
		<BrowserRouter>
			<MuiThemeProvider>
				<App />
			</MuiThemeProvider>
		</BrowserRouter>
	</Provider>
	), document.getElementById('root'));

//console.log("hello from app.js its changing");

