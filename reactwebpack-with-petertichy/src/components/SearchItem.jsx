import React from 'react';
import SearchBar from 'material-ui-search-bar';
 
// ...
class SearchItem extends React.Component{
  constructor(props)
  {
  super(props)
  this.state={
  items:['one','two','three','four','five','six','seven','eight','nine','ten']
  }
  }

render() {
  return(
    <SearchBar dataSource={this.state.items}
      onChange={() => console.log('onChange')}
      onRequestSearch={() => console.log('onRequestSearch')}
      style={{
        margin: '0 auto',
        maxWidth: 800
      }}
    />
  )
}
}

export default SearchItem;