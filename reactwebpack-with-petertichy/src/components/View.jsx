/*import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
 
const View = () => (
  <RaisedButton label="Default" />
);
 
export default View;*/



import React from 'react';
import Part1 from './Part1.jsx';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';




class View extends React.Component{
constructor(props){
	super(props);
	console.log(this.props.match.path);
}

 render(){
 return (
 <div>
    

 <Router>
    <div>
      <ul>
        <li><Link to = {`${this.props.match.url}/part1`}>Part1</Link></li>
      </ul>
      <hr/>
      <Route path = {`${this.props.match.url}/part1`} component={Part1}/>
      
    </div>
  </Router>
  </div>)
  }

};
export default View;

