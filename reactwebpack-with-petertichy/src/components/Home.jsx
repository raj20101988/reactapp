import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { Link, Route, BrowserHistory} from 'react-router-dom';
import LoginPage from './LoginPage.jsx';


const style = {
  margin: 12,
};

class Home extends React.Component{
	constructor(props){
	super();
	console.log(props.match.url);
    console.log(props.match.path);
    console.log(props.match.isExact);
    console.log(props.match.pathname);
    console.log(props.match.params);

	this.login=this.login.bind(this);
	}
	login(){
	alert("okkkkkkk");
	this.props.history.push('/login');
	}
	render(){
		return(
		  <div>

		  <button onClick={this.login}> log in Via JS Event </button>

		  <div>
		  <p> Manage your book.</p>
		  <p> Test as a user. </p>
		   <RaisedButton label="Login via Link & Route Tag" primary={true} style={style} containerElement={<Link to={this.props.match.url + "login"} />} />
		 </div>
		<div>
		  <Route path={`${this.props.match.url}/login`} component={LoginPage} />
		  </div>
		   </div>
		)
	}
}

export default Home;