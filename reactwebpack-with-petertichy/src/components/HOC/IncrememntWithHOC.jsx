import React from 'react';
const BaseComponent=(WrapedComponent) => {
	return class BaseComponent extends React.Component{
			
			constructor(props)
			{
			super(props);
			this.state={
			counter:0
			}
			this.increamentCounter=this.increamentCounter.bind(this);
			}

			increamentCounter()
			{
				this.setState({counter:this.state.counter+1});
			}

			render()
			{
			return (<div className="prnt">
					<WrapedComponent increament={this.increamentCounter} {...this.state} />
				</div>)
			}
	}
	}

//functional component
const Button=(props) => {

	return(<button onClick={props.increament} > Click this button {props.counter} </button>);
}
// class component 
/*class Button extends React.Component {
	//constructor(props) //here is no need of constructor and super.
	//{
	//super(props);
	//}
	render(){
	return(<button onClick={this.props.increament} > Click this button {this.props.counter} </button>);
	}
}*/

const ExtendedButton = BaseComponent(Button);

class IncrememntWithHOC extends React.Component
{
	constructor(props)
	{
	super(props)
	} 
	render()
	{
	return (<ExtendedButton />);
	}
	
}
export default IncrememntWithHOC