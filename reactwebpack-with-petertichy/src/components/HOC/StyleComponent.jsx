import React from 'react';
 const StyleComponent = (WrapedComponent) => {
	
	return class extends React.Component{
		constructor(props){
		super(props);
		}
		render(){
			var styles={
				width:'200px',
				height:'200px',
				background:'red'
			}
			return <WrapedComponent {...this.props} styles={styles} />
		}
	}
}

export default StyleComponent