import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import React from 'react';

class SimpleTable extends React.Component{
	constructor(props)
	{
	super(props);
	}

	render(){
	var products = [{
      id: 1,
      name: "Product1",
      price: 120
  }, {
      id: 2,
      name: "Product2",
      price: 80
  }];
	return(
		<BootstrapTable data={products} striped hover>
		      <TableHeaderColumn isKey dataField='id'>Product ID</TableHeaderColumn>
		      <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
		      <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
		  </BootstrapTable>
	)
	}
}

export default SimpleTable;