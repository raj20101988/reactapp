import React from 'react';
import Main from './Main.jsx';
import Header from './Header.jsx';

const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)
export default App