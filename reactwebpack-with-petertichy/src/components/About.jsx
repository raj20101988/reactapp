
/*import React from 'react';
const About = (props) => {
  return <div> Rajnish kumar kushawaha </div>
}
export default About ;*/


import React from 'react'
import SimpleTable from './SimpleTable.jsx';
import SortTable from './SortTable.jsx';
import StyleComponent from './HOC/StyleComponent.jsx';
import IncrememntWithHOC from './HOC/IncrememntWithHOC.jsx';
import Collapse from './Collapse.jsx';
import SearchItem from './SearchItem.jsx';

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

const About = ({ match }) => (
  <Router>
    <div>

    <SimpleTable />
    <SortTable />
    <Collapse />
    <SearchItem />
      <ul>
        <li><Link to={`${match.url}/part1`}>Part1</Link></li>
        <li><Link to={`${match.url}/part2`}>Part2</Link></li>
        <li><Link to={`${match.url}/topics`}>Topics</Link></li>
      </ul>

      <hr/>
      <IncrememntWithHOC />
      

      <Route path={`${match.url}/part1`} component={extendedPart1}/>
      <Route path={`${match.url}/part2`} component={extendedPart2}/>
      <Route path={`${match.url}/topics`} component={Topics}/>
    </div>
  </Router>
)

const Part1 = (props) => {
  return(<div>
    <h2>Part1</h2>
    <p style={props.styles}> style this text paragraph </p>
  </div>)
}

const extendedPart1 = StyleComponent(Part1)


const Part2 = (props) => {
  return(<div>
    <h2>Part2</h2>
    <p style={props.styles}> style this text other paragraph </p>
  </div>)
}
const extendedPart2 = StyleComponent(Part2)

const Topics = ({ match }) => (
  <div>
    <h2>Topics</h2>
    <ul>
      <li>
        <Link to={`${match.url}/rendering`}>
          Rendering with React
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/components`}>
          Components
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/props-v-state`}>
          Props v. State
        </Link>
      </li>
    </ul>

    <Route path={`${match.url}/:topicId`} component={Topic}/>
    <Route exact path={match.url} render={() => (
      <h3>Please select a topic.</h3>
    )}/>
  </div>
)

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
)

export default About