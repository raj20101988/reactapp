import { Switch, Route, Redirect} from 'react-router-dom';
import React from 'react';
import Home from './Home.jsx';
import About from './About.jsx';
import View from './View.jsx';
import LoginPage from './LoginPage.jsx';
import WelcomeUser from './WelcomeUser.jsx';



class Main extends React.Component{
constructor(props){
	super();
}

isLoggedIn(){
	if(sessionStorage.getItem("logedIn")==="true"){
	return true
	}else{
	return false
	}
}
 render(){
 //console.log("fdfd==="+isLoggedIn());
 return (<div>
    
     <Route path="/" component={Home}/>
      <Route path="/login" component={LoginPage} />
      <Route path="/about" component={About} />
      <Route path="/view" component={View}/>
      <Route path="/welcome" component={WelcomeUser} />
      
      
    
  </div>)
  } 
};
export default Main;

