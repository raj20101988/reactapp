import React from 'react';
import { Link } from 'react-router-dom';
import {Tabs, Tab} from 'material-ui/Tabs';
const Header = () => (
  <Tabs inkBarStyle={{background: 'blue'}}>
   <Tab value={0} label="Home" containerElement={<Link to="/"/>} />
   <Tab value={1} label="About" containerElement={<Link to="/about"/>}/>
   <Tab value={2} label="View" containerElement={<Link to="/view"/>} />
 </Tabs>
)
export default Header