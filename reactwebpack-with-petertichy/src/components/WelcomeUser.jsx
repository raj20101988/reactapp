import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
//import { Link, Route, BrowserHistory} from 'react-router-dom';
import { Link, Route, withRouter} from 'react-router-dom';
import TableDetail from './TableDetail.jsx';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import * as actions from '../actions/userActions';

const styles = {
  propContainer: {
    width: 200,
    overflow: 'hidden',
    margin: '20px auto 0',
  },
  propToggleHeader: {
    margin: '20px auto 10px',
  },
};

class WelcomeUser extends React.Component{ 
  constructor(props)
  {
  super();
  //alert("okkk="+);
  //console.log(this.props.user+'=wel='+props.match.path);
  
 this.state = {
  showCheckboxes: true,
   // user:this.props.user,
    selectedStudents: [],
    id:'',
    name:'',
    status:'',
    address:''
  };
 this.addUserHandler=this.addUserHandler.bind(this);
 this.deleteHandler=this.deleteHandler.bind(this);
 this.changeInput=this.changeInput.bind(this);
 this.viewAboutTable=this.viewAboutTable.bind(this);
 this.handleRowSelection = this.handleRowSelection.bind(this);
 this.isSelected = this.isSelected.bind(this);
 this.handleToggle=this.handleToggle.bind(this);
  }

  componentDidMount() {
    if (this.props.user[0].id == '') {
    alert('componentWillMount');
      this.props.actions.loadUsers();
    }
  }




   handleToggle(event, toggled){
    this.setState({
      [event.target.name]: toggled,
    });
  };

// open table detail component via Route
viewAboutTable(){
console.log(this.props.history.location);
 // this.props.history.push('/welcome/tableDetail'); // this is static url path method for routing.
  this.props.history.push(this.props.match.path+'/tableDetail'); // this is dynamic url path method for routing.

}

// on add User button clicked
addUserHandler()
{
  //alert('okk');
  
console.log('id:'+this.state.id)

  let userInfo={id:this.state.id,name:this.state.name,status:this.state.status,address:this.state.address};
  //var userInfo=this.props.user.push({id:this.state.id,name:this.state.name,status:this.state.status,address:this.state.address});
  this.props.actions.addUser(userInfo)
  //this.setState({user:userInfo});
}

deleteHandler()
{
//const arr=this.state.selectedStudents;
console.log('this.state.selectedStudents=='+this.state.selectedStudents);
  this.props.actions.deleteUser(this.state.selectedStudents);
  this.setState({ selectedStudents : [] })
  console.log('this.state.showCheckboxes='+this.state.showCheckboxes);
}


changeInput(e){
  var type=e.target.getAttribute('type');
  //alert('llll'+e.target.getAttribute('type'));
  switch(type){
  case 'id':
  this.setState({id:e.target.value})
  break;
  case 'name':
  this.setState({name:e.target.value})
  break;
  case 'status':
  this.setState({status:e.target.value})
  break;
  case 'address':
  this.setState({address:e.target.value})
  break;
  }

}

  /*handleRowSelection(selectedRows){
  console.log('selectedRows='+selectedRows);
    this.setState({
      selected: selectedRows,
    });

  };*/


handleRowSelection (selectedRows) {
        let selectedObjs = [];
        selectedObjs.push(...selectedRows);
        console.log('selectedObjs='+selectedObjs);
        this.setState({ selectedStudents : selectedObjs })
    }


 isSelected(index){
    console.log('index='+this.state.selectedStudents.indexOf(index));
    return this.state.selectedStudents.indexOf(index) !== -1;
  };

  render(){
console.log('this.state.selected'+this.state.selectedStudents);
  //alert(this.props.user[0].name);

//if you are using map then must use fat arrow function instead of arrow function because it generates syntax errors
//alert('this.props.user='+this.props.user);
const eachRow = this.props.user.map((item,ind)=>{
  return (
    <TableRow key={ ind } selected={this.isSelected(ind)} >
          <TableRowColumn>{item.id}</TableRowColumn>
          <TableRowColumn>{item.name}</TableRowColumn>
          <TableRowColumn>{item.status}</TableRowColumn>
          <TableRowColumn>{item.address}</TableRowColumn>
    </TableRow>
  )
})

    return(<div>
        <TextField type="id"
          floatingLabelText="User Id"
           onChange={this.changeInput}
        /><br />
        <TextField

          floatingLabelText="User Name" type="name"
           onChange={this.changeInput}
        /><br />
         <TextField
          floatingLabelText="User Status" type="status"
           onChange={this.changeInput}
        /><br />
        <TextField
          floatingLabelText="User Address" type="address"
           onChange={this.changeInput} />
         <RaisedButton label="Add User" primary={true} onClick={this.addUserHandler} />
         



         <Table onRowSelection={this.handleRowSelection} multiSelectable={true}>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Name</TableHeaderColumn>
            <TableHeaderColumn>Status</TableHeaderColumn>
            <TableHeaderColumn>Address</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody deselectOnClickaway={false}>{ eachRow }</TableBody>
        </Table>

        <RaisedButton label="Delete User" primary={true} onClick={this.deleteHandler} />
        <RaisedButton label="Table Details" primary={true} onClick={this.viewAboutTable} />

        <h3 style={styles.propToggleHeader}>Multiple Properties</h3>
          <Toggle
            name="showCheckboxes"
            label="Show Checkboxes"
            onToggle={this.handleToggle}
            defaultToggled={this.state.showCheckboxes}
          />

          <div>
          <Route path={`${this.props.match.url}/tableDetail`} component={TableDetail} />
          </div>
      </div>);
  };
}


function mapStateToProps(state, ownProps) {
    /*return {
      user: state
    };*/

if (state.length > 0) {
    return {
      user: state
    };
  } else {
    return {
      user: [{id:'', name:'', status:'', address:''}]
    }
  }


}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(WelcomeUser));



//export default WelcomeUser;