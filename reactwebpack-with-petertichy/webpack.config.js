var  HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack=require('webpack');
console.log('process.env.NODE_ENV=='+ process.env.NODE_ENV);
var isProd=false
/*
var isProd=process.env.NODE_ENV==='production'; // true / false

var cssDev=['style-loader','css-loader','sass-loader'];
var cssProd=ExtractTextPlugin.extract({
          fallback: "style-loader",  
          use: [ 'css-loader', 'sass-loader'], // these are used to load css and sass files
          publicPath:'/dist/css'
        });


var cssConfig=isProd?cssProd:cssDev;*/

console.log('isProd='+isProd);

module.exports={
  //entry:{ app:'./src/index.js', contact:'./src/contact.js'},
   entry:'./src/index.js',
  output:{
    path: __dirname + '/dist',
    //filename: 'js/[name].js',
    filename: 'js/bundle.js',
    publicPath:'/'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        /*use: [                        // use 'use' from webpack 2 and onwards, use loaders in previous version.
          { loader: "style-loader" },   // it is used to use css in the main html with style tag <style>
          { loader: "css-loader" }      // it is used to load css files using link tag.
        ]*/
        //use: [ 'style-loader', 'css-loader', 'sass-loader'] //this is used for development way

        use:ExtractTextPlugin.extract({
          fallback: "style-loader",  
          use: [ 'css-loader', 'sass-loader'] // these are used to load css and sass files for production
        }),

        //use: cssConfig
      },
      { test: /\.(js|jsx)$/,
       exclude: /node_modules/,
        loader: "babel-loader",
        query: {
               presets: ['env', 'react']
            } 
      } //for  loading and using babel
    ]
  },
  devServer: {
  contentBase: path.join(__dirname, "dist"),
  historyApiFallback: true,
  compress: true, // it will compress the files
  port: 9000,  // it will serves at localhost:9000 instead of default 8080
  hot:true,  // for hot loading,i.e. changes in js or css will automatically update the browser.
  stats:"errors-only",  //it will hide all the webpack package building process like  Asset, Size, Chunks,  Chunk Names etc.
  open:true  // it will open the app in new window each time when any file gets any changes.
},
  plugins: [ new HtmlWebpackPlugin({  // html-webpack-plugin is used to genetate defaule html or supplied html using templete and include js file inside dist folder automatically
      title: 'React project index',
      minify: {
        collapseWhitespace: true  // minify html file using webpack
      },
      hash: true, // this is used to randomly generate  spacial character after .js in javascript file on each render ,
                  // you can see this on cmd during running the webpack development command and in renderd html using inspenct element on browser. 
      excludeChunks:['contact'], // this is used to exclude contact.js file from index.html file at runtime on dev or prod command.
     //filename: './../index.html', //this is used to write the index.html file in the folder wherever you want means in dist or on dist level.
                                  //and all the needed file will be included automatically like css, scss or js file which was build
      
      template:'./src/index.ejs' // ejs (extended javascript template) you can write .ejs or .html extension these are used for template.
  }),
  new HtmlWebpackPlugin({  // html-webpack-plugin is use to genetate default html or supplied html using templete and include js file inside dist folder automatically
      title: 'React project contact',
      /*minify: {
        collapseWhitespace: true  // minify html file using webpack
      },*/
      hash: true, // this is used to randomly generate  spacial character after .js in javascript file on each render ,
                  // you can see this on cmd during running the webpack development command and in renderd html using inspenct element on browser. 
     chunks: ['contact'], // chunks is used to include the contact js file in the contact html generated here.
     filename: 'contact.html', //this is used to write the index.html file in the folder wherever you want means in dist or on dist level.
                                  //and all the needed file will be included automatically like css, scss or js file which was build
      
      template:'./src/contact.html' // ejs (extended javascript template) 
  }),

    /*new ExtractTextPlugin({
    filename:"styles.css",
    //disable:!isProd,
    disable:!isProd,
    allChunks:true
    }),*/ 
    new ExtractTextPlugin("css/styles.css"),// it will generate styles.css inside dist/css and include to index.html file using link tag.
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  
  ]
}